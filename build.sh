#!/usr/bin/env bash

# get libraries and generate data.js
# go get -v ./...
pushd parser
go run main.go > ../data.js
popd

# if a error occured when running parser, do not commit
if [ $? -ne 0 ]; then
    echo "Do not commit, error during parsing!"
    exit 0
fi

# compare the two files
cmp data.js public/js/data.js

# if they are no different, just exit 0
if [ $? -eq 0 ]; then
    echo "Do not commit, same file!"
    exit 0
fi

# they are different, so push the new file
git checkout master
mv data.js public/js/data.js
git add public/js/data.js
git commit -m 'uploaded new data.js'
git push origin master

echo "Committed new data.js"
