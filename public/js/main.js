var app = new Vue({
    el: '#app',
    data: {
        facilities: akuinfo.facilities,
        rooms: akuinfo.rooms,
        numbercomplexes: akuinfo.complexes.length,
        checkedRequirements: [],
        checkedRooms: [],
        priceFrom: 0,
        priceTo: 999999,
        areaFrom: 0,
        areaTo: 999999,
    },
    filters: {
        capitalize: function (value) {
            if (!value) {
                return ''
            }

            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        }
    },
    methods: {
        search: function(event) {
            updateMarkers(this.checkedRequirements, this.checkedRooms, this.priceFrom, this.priceTo, this.areaFrom, this.areaTo);
        },
    },
})

function checkRange(x1, x2, y1, y2) {
    return ((x1 <= y2 && y1 <= x2) ? true : false);
}

function updateMarkers(requirements, checkedRooms, priceFrom, priceTo, areaFrom, areaTo) {
    // not dealing with decimals, 100,25 => 10025
    priceFrom = priceFrom * 100
    priceTo = priceTo * 100
    areaFrom = areaFrom * 100
    areaTo = areaTo * 100

    // remove any previous markers we have set
    clearMarkers();

    // loop each apartment complex, and check if
    // it follows the requirements
    for(let i = 0; i < akuinfo.complexes.length; i++) {
        let currentComplex = akuinfo.complexes[i];

        // check price and area first
        let requirementsPriceSatisfied = false;
        for(let k = 0; k < currentComplex.types.length; k++) {
            if(checkRange(priceFrom, priceTo, currentComplex.types[k].pricefrom, currentComplex.types[k].priceto)) {
                requirementsPriceSatisfied = true;
                break;
            } 
        }
        if(requirementsPriceSatisfied == false) {
            continue;
        }

        let requirementsAreaSatisfied = false;
        for(let k = 0; k < currentComplex.types.length; k++) {
            if(checkRange(areaFrom, areaTo, currentComplex.types[k].areafrom, currentComplex.types[k].areato)) {
                requirementsAreaSatisfied = true;
                break;
            }
        }
        if(requirementsAreaSatisfied == false) {
            continue;
        }

        // check rooms requirement
        if(checkedRooms.length > 0) {
            requirementsSatisfied = false;
            for(let j = 0; j < checkedRooms.length; j++) {
                for(let k = 0; k < currentComplex.types.length; k++) {
                    if(currentComplex.types[k].rooms == checkedRooms[j]) {
                        requirementsSatisfied = true;
                        break;
                    }
                }
            }

            if(requirementsSatisfied == false) {
                continue
            }
        }

        // used to see if it satifies our requirements (if it can)
        requirementsSatisfied = true;
        // loop our requirements
        for(let j = 0; j < requirements.length; j++) {
            let currentRequirement = requirements[j];

            // loop all apartment complex facilities
            for(let key in currentComplex.facilities) {
                // only check when the key is equal
                if(currentRequirement !== key) {
                    continue;
                }

                let currentFacility = currentComplex.facilities[key];

                // if it is 0, then it is confirmed to not have it
                if(currentFacility.has == 0) {
                    requirementsSatisfied = false;
                    break;
                }
            }
        }

        if(requirementsSatisfied == false) {
            continue;
        }

        // find out if we are dealing with a complex that have a facility, have description or does not mention it
        let requirementSoft = 1;
        let requirementCounter = 0;
        // determine what type the icon should be
        for(let j = 0; j < requirements.length; j++) {
            let currentRequirement = requirements[j];

            // loop all apartment complex facilities
            for(let key in currentComplex.facilities) {
                // only check when the key is equal
                if(currentRequirement !== key) {
                    continue
                }

                let currentFacility = currentComplex.facilities[key];

                // if it is 0, then it is confirmed to not have it
                if(currentFacility.has == 1 && requirementSoft != -1) {
                    requirementSoft = 1;
                } else if(currentFacility.has == -1) {
                    requirementSoft = -1;
                }

                requirementCounter += 1;
            }
        }

        if(requirementCounter != requirements.length) {
            requirementSoft = -2;
        }         

        // add our marker to our map
        addMarker(currentComplex, requirements, requirementSoft);
    }
}

var openinfowindow = null;
var markers = [];
function addMarker(complex, requirements, softSatisfy) {
    // fix our popup html
    popupHtml = "";
    popupHtml += '<div class="row">';
    popupHtml +=    '<div class="column uk-padding-small uk-padding-remove-vertical uk-padding-remove-left"><img src="'+akuinfo.baseurl+complex.imglink+'"/></div>'
    popupHtml +=    '<div class="column">';
    popupHtml +=       '<b>'+complex.title+'</b></br>';
    popupHtml +=       complex.address+'</br></br>';

    for(let i = 0; i < complex.types.length; i++) {
        let currentType = complex.types[i];
        popupHtml +=   '<b>Room(s) '+currentType.rooms+'</b></br>';
        popupHtml +=   '<div class="uk-margin-left">Price: '+currentType.pricefrom/100+' - '+currentType.priceto/100+' DKK</div>';
        popupHtml +=   '<div class="uk-margin-left">Area: '+currentType.areafrom/100+' - '+currentType.areato/100+' m2</div>';
    }

    popupHtml +=       '</br>';

    // loop over the requirements that have been set, and give the string
    for(let i = 0; i < requirements.length; i++) {
        let key = requirements[i];

        let found = false;
        for(let facilityKey in complex.facilities) {
            if(key !== facilityKey) {
                continue;
            }

            found = true;
            popupHtml += '<b>'+app.$options.filters.capitalize(key)+'</b>: '+complex.facilities[facilityKey].string+'</br>';
        }

        if(!found) {
            popupHtml += '<b>'+app.$options.filters.capitalize(key)+'</b>: n/a</br>';
        }
    }
    popupHtml +=       '</br>';
    popupHtml +=       'Se på AKU-Aalborg <a href="'+akuinfo.baseurl+complex.link+'" target="_blank">her</a>';
    popupHtml +=    '</div>';
    popupHtml += '</div>';

    // everything related to our marker!
    let infowindow = new google.maps.InfoWindow({
        content: popupHtml,
        maxWidth: 500,
    });

    var icon;
    switch(softSatisfy) {
        case -2:
            icon = "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png";
            break;
        case -1:
            icon = "http://maps.google.com/mapfiles/ms/icons/purple-dot.png";
            break;
        case 1:
            icon = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
            break;
        default:
            alert("Should not happen");
            break;
    }

    let marker = new google.maps.Marker({
        position: {lat: complex["latlng"]["lat"], lng: complex["latlng"]["lng"]},
        map: map,
        title: complex["address"],
        icon: {url: icon},
    });

    // when we click it, hide the precious if any, and show the new one
    marker.addListener('click', function() {
        if(openinfowindow !== null) {
            openinfowindow.set("marker", null);
            openinfowindow.close();
            openinfowindow = null;
        }
        infowindow.open(marker.get('map'), marker);
        openinfowindow = infowindow;
    });

    markers.push({"marker": marker, "infowindow": infowindow});
}

function clearMarkers() {
    // remove all markers and reset array
    for(i = 0; i < markers.length; i++) {
        markers[i].marker.setMap(null);
    }
    markers = []

    // hide any popups, and set to null
    if(openinfowindow !== null) {
        openinfowindow.set("marker", null);
        openinfowindow.close();
        openinfowindow = null;
    }
}

// initialise our map
var map
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 57.048820, lng: 9.921747}, // set location to Aalborg centrum
        zoom: 12, 
        disableDefaultUI: true, // disable all ui
    });

    // manual load first time
    google.maps.event.addListenerOnce(map, 'idle', function () {
        updateMarkers([], [], 0, 9999999, 0, 9999999);
    });
}
