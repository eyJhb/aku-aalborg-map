module gitlab.com/eyJhb/aku-aalborg-map/parser

go 1.16

require (
	github.com/anaskhan96/soup v1.2.4
	github.com/kr/pretty v0.2.1
	googlemaps.github.io/maps v1.3.2
)
