package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/anaskhan96/soup"
	"github.com/kr/pretty"
	"googlemaps.github.io/maps"
)

type akuInfo struct {
	BaseUrl    string                   `json:"baseurl"`
	Complexes  []Complex                `json:"complexes"`
	Facilities map[string]*FacilityStat `json:"facilities"`
	Rooms      map[string]int           `json:"rooms"`
}

type FacilityStat struct {
	Yes     int `json:"yes"`
	No      int `json:"no"`
	Unknown int `json:"unknown"`
}

type Complex struct {
	Title      string              `json:"title"`
	Link       string              `json:"link"`
	ImgLink    string              `json:"imglink"`
	Address    string              `json:"address"`
	Latlng     LatLng              `json:"latlng"`
	Types      []PropertyType      `json:"types"`
	Facilities map[string]Facility `json:"facilities"`
}

type LatLng struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type Facility struct {
	String string `json:"string"`
	Has    int8   `json:"has"`
}

type PropertyType struct {
	Link               string `json:"link"`
	Rooms              string `json:"rooms"`
	PriceFrom          int32  `json:"pricefrom"`
	PriceTo            int32  `json:"priceto"`
	AreaFrom           int32  `json:"areafrom"`
	AreaTo             int32  `json:"areato"`
	NumberOfProperties int16  `json:"numberofproperties"`
}

func simpleRequest(endpoint string) (string, error) {
	res, err := http.Get("https://vl.aku-aalborg.dk/" + endpoint)
	if err != nil {
		return "", err
	}

	resBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return string(resBytes), nil
}

func parseAddress(address string) string {
	address = strings.TrimSpace(address)
	address = strings.Replace(address, "  ", " ", -1)
	address = strings.Replace(address, "\t", "", -1)
	address = strings.Replace(address, "\n", ";", -1)
	address = strings.Replace(address, ";;", "", -1)
	address = strings.Replace(address, "+", "-", -1)
	address = strings.Replace(address, " - ", "-", -1)

	split := strings.Split(address, ";")
	street := split[0]
	city := split[1]

	// if strings.Contains(address, "/") || strings.Contains(address, "+") {
	// allways get the last after a /, e.g. Peder Barkesgade 17/Danmarksgade 35
	if strings.Contains(address, "/") {
		address = strings.Replace(address, " / ", "/", -1)
		address = strings.Replace(address, " + ", "+", -1)

		splitStreet := strings.Split(street, "/")
		lastStreet := splitStreet[len(splitStreet)-1]

		// we might just have gotten a number... e.g. Stjernepladsen 19/27
		if len(lastStreet) < 5 {
			lastStreet = splitStreet[len(splitStreet)-2]
		}

		address = lastStreet + ";" + city
	}

	// strip everything after -
	if strings.Contains(address, "-") {
		address = street[:strings.Index(street, "-")] + ";" + city
	}

	address = strings.TrimSpace(address)
	address = strings.Replace(address, ";", ", ", -1)

	return address
}

func GetComplexes() ([]Complex, error) {
	var properties []Complex

	mainHtml, err := simpleRequest("BoligOversigt.aspx?ID=0")
	if err != nil {
		return nil, err
	}

	doc := soup.HTMLParse(mainHtml)
	docProperties := doc.FindAll("div", "class", "ejendom")

	for _, prop := range docProperties {
		title := strings.TrimSpace(prop.Find("div", "class", "overskrift").FullText())
		link := prop.Find("div", "class", "foto").Find("a").Attrs()["href"]
		imglink := prop.Find("div", "class", "foto").Find("img").Attrs()["src"]
		address := parseAddress(prop.Find("div", "class", "adresse").FullText())
		lat, lng := getLatLng(address)
		if lat == 0.0 || lng == 0.0 {
			return nil, errors.New("the lat,lng are 0,0 - this cannot be true")
		}

		properties = append(properties, Complex{
			Title:   title,
			Link:    link,
			ImgLink: imglink,
			Address: address,
			Latlng: LatLng{
				Lat: lat,
				Lng: lng,
			},
		})
	}

	return properties, nil
}

func GetComplexDetails(prop Complex) (Complex, error) {
	propHtml, err := simpleRequest(prop.Link)
	if err != nil {
		return prop, err
	}

	docProp := soup.HTMLParse(propHtml)
	boligTable := docProp.Find("div", "class", "boligtyper")
	boligTrs := boligTable.FindAll("tr", "class", "tblbody")

	for _, boligTr := range boligTrs {
		boligTd := boligTr.FindAll("td")

		roomsString := boligTd[0].FullText()

		var rooms string
		if strings.HasPrefix(roomsString, "Værelse") {
			rooms = "1"
		} else {
			rooms = string(roomsString[0])
			if strings.Contains(roomsString, "½") {
				rooms += ".5"
			}
		}

		pricefrom, _ := strconv.ParseInt(strings.Replace(strings.Replace(boligTd[1].Text(), ".", "", -1), ",", "", -1), 10, 32)
		priceto, _ := strconv.ParseInt(strings.Replace(strings.Replace(boligTd[2].Text(), ".", "", -1), ",", "", -1), 10, 32)
		areafrom, _ := strconv.ParseInt(strings.Replace(strings.Replace(boligTd[3].Text(), ".", "", -1), ",", "", -1), 10, 32)
		areato, _ := strconv.ParseInt(strings.Replace(strings.Replace(boligTd[4].Text(), ".", "", -1), ",", "", -1), 10, 32)
		numberofproperties, _ := strconv.ParseInt(boligTd[5].Text(), 10, 16)

		prop.Types = append(prop.Types, PropertyType{
			Link:               boligTd[0].Find("a").Attrs()["href"],
			Rooms:              rooms,
			PriceFrom:          int32(pricefrom),
			PriceTo:            int32(priceto),
			AreaFrom:           int32(areafrom),
			AreaTo:             int32(areato),
			NumberOfProperties: int16(numberofproperties),
		})
	}

	// map facilities
	facilitiesMap := make(map[string]Facility)
	faciliteterTable := docProp.Find("div", "class", "faciliteter")
	faciliteterTrs := faciliteterTable.FindAll("tr", "class", "tblbody")
	for _, faciliteterTr := range faciliteterTrs {
		faciliteterTd := faciliteterTr.FindAll("td")
		key1 := strings.ToLower(faciliteterTd[0].FullText())
		val1 := strings.ToLower(faciliteterTd[1].FullText())
		key2 := strings.ToLower(faciliteterTd[3].FullText())
		val2 := strings.ToLower(faciliteterTd[4].FullText())
		// has => -1 => could not parse it, 0 => does not have it, 1 => does have it

		facilitiesMap[key1] = Facility{
			String: val1,
			Has:    doesHaveItParser(val1),
		}
		facilitiesMap[key2] = Facility{
			String: val2,
			Has:    doesHaveItParser(val2),
		}
	}

	prop.Facilities = facilitiesMap

	return prop, nil
}

func doesHaveItParser(inp string) int8 {
	if strings.Contains(inp, "ja") {
		return 1
	} else if strings.Contains(inp, "nej") {
		return 0
	}

	return -1
}

var googleMapsClient *maps.Client

func getLatLng(address string) (float64, float64) {
	var err error
	if googleMapsClient == nil {
		googleMapsClient, err = maps.NewClient(maps.WithAPIKey("AIzaSyCq0WIXE7QN9lWiw4EL7MlrotNDuK6S6M8"))
		if err != nil {
			log.Fatalf("fatal error: %s", err)
		}
	}

	r := &maps.GeocodingRequest{
		Address: address,
	}
	geocode, err := googleMapsClient.Geocode(context.Background(), r)
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}

	if len(geocode) == 0 {
		fmt.Printf("No geocode found for: %s\n", address)
		pretty.Println(geocode)
		return 0, 0
	}

	return geocode[0].Geometry.Location.Lat, geocode[0].Geometry.Location.Lng
}

func main() {
	properties, err := GetComplexes()
	if err != nil {
		log.Fatal(err)
		return
	}

	// used to get a total list of facilities
	facilitiesStats := make(map[string]*FacilityStat)
	roomsCount := make(map[string]int)
	for i, prop := range properties {
		propDetails, err := GetComplexDetails(prop)
		if err != nil {
			log.Fatal(err)
		}

		// count facilities and get all different facilities
		for key, val := range propDetails.Facilities {
			if key == "" {
				continue
			}

			if _, ok := facilitiesStats[key]; !ok {
				facilitiesStats[key] = &FacilityStat{0, 0, 0}
			}

			switch val.Has {
			case -1:
				facilitiesStats[key].Unknown += 1
			case 0:
				facilitiesStats[key].No += 1
			case 1:
				facilitiesStats[key].Yes += 1
			}
		}

		// count rooms and get how many different
		for _, complexType := range propDetails.Types {
			if _, ok := roomsCount[complexType.Rooms]; !ok {
				roomsCount[complexType.Rooms] = 0
			}
			roomsCount[complexType.Rooms] += 1
		}

		properties[i] = propDetails
	}

	aku := akuInfo{
		BaseUrl:    "https://vl.aku-aalborg.dk/",
		Complexes:  properties,
		Facilities: facilitiesStats,
		Rooms:      roomsCount,
	}

	json, err := json.Marshal(aku)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("var akuinfo = %s\n", json)
}
