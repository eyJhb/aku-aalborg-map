# AKU-Aalborg Map
The map can be found [here](https://eyjhb.gitlab.io/aku-aalborg-map/), and is created because of the poor user experience at AKU-Aalborg's own website.
Them map aims to give easy to use filters and to show where the apartments are geographically.
This also enables newcomers to see, what supermarkets are in the area, fitness, how the university is in relation to the apartment, etc.

Any issues or pull requests are welcome!
